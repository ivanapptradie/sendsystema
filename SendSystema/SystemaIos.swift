//
//  Systema.swift
//  iosApp
//
//  Created by Ren Decano on 29/8/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import shared
import UIKit

public typealias TagMap = [String: Any] // Same with tagMapping on android
typealias tags = [TagMap]

public class SystemaIos {
  private let proxyApiKey = "PMAK-6113bbd984433f0046e30785-fe032bc85b42f8e1eff7507f1c23d592b9"
  private var proxyUrls = [
    // . EndpointType.dynamicconfig: "https://daef600f-b1c2-49fc-a739-c56ad0c74ca1.mock.pstmn.io",
    EndpointType.tracker: "https://853f87e6-a3ef-41d2-b098-8a9153a06315.mock.pstmn.io",
    EndpointType.recommend: "https://853f87e6-a3ef-41d2-b098-8a9153a06315.mock.pstmn.io",
    EndpointType.search: "https://853f87e6-a3ef-41d2-b098-8a9153a06315.mock.pstmn.io"
  ]

  let tagMapping = [
    SystemaTags().ProductId: 1,
    SystemaTags().RecId: 2,
    SystemaTags().ProductUrl: 3,
    SystemaTags().ResultId: 4,
    SystemaTags().Visible: 5,
    SystemaTags().Observed: 6,
    SystemaTags().ContainerUrl: 7,
  ]

  public init(
    with apiKey: String,
    and endpointURL: String
  ) {
    let proxyUrls2 = [
      // . EndpointType.dynamicconfig: "https://daef600f-b1c2-49fc-a739-c56ad0c74ca1.mock.pstmn.io",
      EndpointType.tracker: endpointURL,
      EndpointType.recommend: endpointURL,
      EndpointType.search: endpointURL
    ]
    SystemaAIClient().initialize(
      clientID: "unreal",
      apiKey: apiKey,
      environment: EnvironmentType.test,
      logLevel: SystemaLogLevel.debug,
      proxyUrls: proxyUrls2,
      meta: [SystemaConstants().SystemaTagMapping: tagMapping], completionHandler: { val, _ in
        print("------ SYSTEMA INIT START ------")
        print(val)

        guard let retValue = val,
          let userId = "systema_demo_user".toBase64()
        else {
          return
        }

        SDK.shared.systemaAi = retValue

        retValue.setUserIdHash(uid: userId, completionHandler: { _, error in
          guard error == nil else { return }
          print("------ SYSTEMA INIT SET USER ------")
        })
        print("------ SYSTEMA INIT END ------")
      }
    )
  }

  public func getInstance() -> SystemaAI? {
    guard let api = SDK.shared.systemaAi else {
      return nil
    }
    return api
  }

  // TODO: Remove this
  public func testAPI() {
    guard let api = getInstance() else {
      return
    }

    let dummyRequest = RecommendationRequest(environment: nil, user: nil, id: nil, category: nil, size: nil, start: nil, filter: nil, exclusion: nil, paginationTimestamp: nil, language: nil, display: nil, displayVariants: nil, meta: nil)

    api.getTrending(
      payload: SDK.shared.dummyReq,
      requestOptions: nil,
      completionHandler: {
        val, _ in
        print("------ TRENDING RESPONSE START ------")
        print(val)
        print("------ TRENDING RESPONSE END------")
      }
    )

    api.getPopular(
      payload: SDK.shared.dummyReq,
      requestOptions: nil,
      completionHandler: {
        val, _ in
        print("------ POPULAR RESPONSE START ------")
        print(val)
        print("------ POPULAR RESPONSE END------")
      }
    )
  }
}
