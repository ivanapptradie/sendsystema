

Pod::Spec.new do |spec|

  spec.name         = "SendSystema"
  spec.version      = "1.1.5"
  spec.summary      = "This is a SendSystema framework"
  spec.description  = "TBD. To do description for a is a SendSystema framework"

  spec.homepage     = "https://gitlab.com/ivanapptradie/sendsystema.git"


    spec.license      = { :type => "MIT", :file => 'LICENSE' }
  spec.author             = { "Ivan" => "ivan@apptradies.com" }
  spec.platform     = :ios, "13.4"


  spec.source       = { :git => "https://gitlab.com/ivanapptradie/sendsystema.git", :tag => spec.version.to_s }
  #spec.source_files  = "**/*.{swift}"
  #spec.exclude_files = "SendSystemaTests/*.{swift}"
    
  spec.swift_versions = "5.0"
  
  spec.vendored_frameworks      = "SendSystema/Build/SendSystema.framework"
  
  

  # spec.public_header_files = "Classes/**/*.h"

  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

    
   #spec.libraries                = "c++"
  #spec.framework  = "shared"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  #spec.dependency "shared"

end
