//
//  SystemaView.swift
//  SystemaIosSDK
//
//  Created by John Ivan Lacuesta on 23/9/21.
//

import Foundation
import shared
import UIKit

public class SystemaView: UIView {
  var systemaTagger: SystemaTagger?
  private var product: Product?

  override init(frame: CGRect) {
    super.init(frame: frame)
  }

  convenience init() {
    self.init(frame: CGRect.zero)
  }

  required init(coder aDecoder: NSCoder) {
    fatalError("This class does not support NSCoding")
  }

  func setProduct(_ product: Product) {
    self.product = product
    systemaTagger = SystemaTagger(with: product)
  }
}
