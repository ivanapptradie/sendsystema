//
//  SystemaTracker.swift
//  SystemaIosSDK
//
//  Created by John Ivan Lacuesta on 25/10/21.
//

import Foundation

import shared

public typealias SystemaTrackResponseClosure = (Bool, _ error: Error?) -> Void

public protocol SystemaTracker {}

// MARK: Public functions

extension SystemaTracker {
  public func trackItemClicked(
    for product: Product,
    completion: @escaping SystemaTrackResponseClosure
  ) {
    let tagger = SystemaTagger(with: product)

    SDK.shared.systemaAi?.trackItemClicked(
      productId: tagger.productId,
      url: tagger.productUrl ?? "",
      recId: tagger.recId,
      referrer: tagger.referrerUrl ?? "",
      completionHandler: { val, error in

        if let errorRet = error {
          return completion(false, errorRet)
        }

        guard val != nil else { return completion(false, nil) }

        return completion(true, nil)
      }
    )
  }

  public func trackContainerShown(
    for products: [Product],
    screenName: String,
    completion: @escaping SystemaTrackResponseClosure
  ) {
    guard products.count > 0 else { return }
    let product = products[0]
    let tagger = SystemaTagger(with: product)

    var dict: [[String: String]] = []
    for prod in products {
      let tag = SystemaTagger(with: prod)
      dict.append(["recId": tag.recId])
    }

    let container = ItemContainer(recItems: dict, resultId: tagger.resultId ?? "")

    SDK.shared.systemaAi?.trackContainerShown(
      productId: tagger.productId,
      containers: [container],
      url: tagger.productUrl ?? product.link,
      referrer: tagger.referrerUrl ?? product.link, // TODO: To check with ren what are these values
      completionHandler: { val, error in

        if let errorRet = error {
          return completion(false, errorRet)
        }

        guard val != nil else { return completion(false, nil) }

        return completion(true, nil)
      }
    )
  }

  public func trackPageViewed(
    for product: Product,
    completion: @escaping SystemaTrackResponseClosure
  ) {
    let tagger = SystemaTagger(with: product)

    SDK.shared.systemaAi?.trackPageViewed(
      productId: tagger.productId,
      url: tagger.productUrl ?? "",
      recId: tagger.recId,
      referrer: tagger.referrerUrl ?? "",
      completionHandler: { val, error in

        if let errorRet = error {
          return completion(false, errorRet)
        }

        guard val != nil else { return completion(false, nil) }

        return completion(true, nil)
      }
    )
  }
}
