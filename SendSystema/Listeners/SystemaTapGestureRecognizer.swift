//
//  SystemaTapGestureRecognizer.swift
//  SystemaIosSDK
//
//  Created by John Ivan Lacuesta on 20/9/21.
//

import shared
import UIKit

class SystemaTapGestureRecognizer: UITapGestureRecognizer {
  let systemaTagger: SystemaTagger

  init(target: AnyObject, action: Selector, systemaTagger: SystemaTagger) {
    self.systemaTagger = systemaTagger
    super.init(target: target, action: action)
  }
}
