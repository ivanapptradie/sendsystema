//
//  UIViewController+Listener.swift
//  SystemaIosSDK
//
//  Created by John Ivan Lacuesta on 27/9/21.
//

import Foundation
import UIKit

import shared

extension UIViewController {
  func track(on view: UIView, with product: Product) {
    let tagger = SystemaTagger(with: product)
    print("tracking from controller \(tagger.productId)")
  }

  open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let view = touches.first?.view {
      print("touched view ")
    }
  }
}
